/**
 * Write a function called "rokket" which receives two number arrays of any size. Return a list containing the
 * intersection ofthe two arrays (all unique numbers in both arrays).
 * Example:
 * console.log(rokket([1, 2, 5], [2, 1, 6])) // this outputs [1, 2, 5, 6]
 * console.log(rokket([1, 2, 3], [4, 5, 6])) // this outputs [1, 2, 3, 4, 5, 6]
 */

// console.log(rokket([1, 2, 5], [2, 1, 6])) // this outputs [1, 2, 5, 6]
// console.log(rokket([1, 2, 3], [4, 5, 6])) // this outputs [1, 2, 3, 4, 5, 6]

const rokket = (array1, array2) => console.log([... new Set(array1.concat(array2).sort())])

console.log(rokket([1, 2, 5], [2, 1, 6])) // this outputs [1, 2, 5, 6]
console.log(rokket([1, 2, 3], [4, 5, 6])) // this outputs [1, 2, 3, 4, 5, 6]

