/**
 * Write a function called "rokket" which produces the following output when called:
 * console.log(rokket(2)(5)(3)) // this outputs 30
 * console.log(rokket(4)(2)(2)) // this outputs 16
 * console.log(rokket(8)(2)(1)) // this also outputs 16
 * This function must work for any three integer numbers.
 */

// function rokket(x){
//     return function(y){
//         return function(z){ return x * y * z; }
//     }
// }

const rokket = x => y => z => (Number.isInteger(x) && Number.isInteger(y) && Number.isInteger(z))? x * y * z : 'Los números deben ser enteros';

console.log(rokket(2)(5)(3));
console.log(rokket(4)(2)(2));
console.log(rokket(8)(2)(1));