/**
 * Write a function called "rokket" which receives a list of names from a contact book. Each name is an object
 * consisting of a first name and last name. Return a listthat shows only the last names.
 * https://md2pdf.netlify.com 2/2
 * Example:
 * const contacts = [
 * { firstName: 'Juanito', lastName: 'Rokket' },
 * { firstName: 'James', lastName: 'Bond' },
 * { firstName: 'Harry', lastName: 'Potter' },
 * ]
 * console.log(rokket(contacts)) // this outputs ['Rokket', 'Bond', 'Potter']
 */

const contacts = [
    { firstName: 'Juanito', lastName: 'Rokket' },
    { firstName: 'James', lastName: 'Bond' },
    { firstName: 'Harry', lastName: 'Potter' }
]

var list = [];

const rokket = (contacts) => (contacts.forEach( element => list.push(element.lastName)), console.log(list));

console.log(rokket(contacts));