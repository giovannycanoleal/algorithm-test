/**
 * Write a function called "rokket" which receives an array with several strings. 
 * It must output the longest (characterlength) string in the array.
 * Example:
 * const list = ['best', 'company', 'ever']
 * console.log(rokket(list)) // this outputs 'company'
*/

const list = ['best', 'company', 'ever']

const rokket = (list) => list.reduce((a, b) => a.length > b.length ? a : b, '');

console.log(rokket(list));